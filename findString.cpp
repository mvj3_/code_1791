#include <iostream>
#include <string>

using namespace std;
int main()
{
string sentence = "Nanners maketh man";
	string word = "an";
	int count = 0;
	size_t position = 0;
	for(size_t i = 0; i<sentence.length()-word.length();)
	{
		position = sentence.find(word,i);
		if(position == string::npos)
		{
			break;
		}
		count++;
		i = position+1;
	}
	cout<<"\""<<word<<"\" occurs in \" "<<sentence<<"\" "<<count<<" times";
return 0;
}